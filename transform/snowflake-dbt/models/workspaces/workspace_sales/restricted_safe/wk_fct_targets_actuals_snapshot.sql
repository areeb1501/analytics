WITH dim_date AS (

  SELECT *
  FROM {{ref('dim_date')}}
  
),

targets AS (

  SELECT *
  FROM {{ref('wk_fct_sales_funnel_target_daily_pivoted')}}

),

actuals AS (

  SELECT *
  FROM {{ref('wk_fct_crm_opportunity_daily_snapshot')}}


),

combined AS (

  SELECT 
    -- Keys
    actuals.actuals_targets_pk,
    actuals.dim_crm_opportunity_id,
    actuals.dim_sales_qualified_source_id,
    actuals.dim_order_type_id,
    actuals.dim_order_type_live_id,
    actuals.dim_crm_user_hierarchy_sk,
    actuals.crm_user_business_unit,
    actuals.crm_user_sales_segment,
    actuals.crm_user_geo,
    actuals.crm_user_region,
    actuals.crm_user_area,
    actuals.crm_user_role_name,
    actuals.crm_user_role_level_1,
    actuals.crm_user_role_level_2,
    actuals.crm_user_role_level_3,
    actuals.crm_user_role_level_4,
    actuals.crm_user_role_level_5,
    actuals.crm_user_sales_segment_grouped,
    actuals.crm_user_sales_segment_region_grouped,
    actuals.merged_crm_opportunity_id,
    actuals.dim_crm_account_id,
    actuals.dim_crm_person_id,
    actuals.sfdc_contact_id,
    actuals.record_type_id,
    actuals.dim_crm_opp_owner_stamped_hierarchy_sk,
    actuals.technical_evaluation_date_id,
    actuals.ssp_id,
    actuals.ga_client_id,
    actuals.report_user_segment_geo_region_area_sqs_ot,
    actuals.sales_qualified_source_name,
    actuals.opp_owner_name,
    actuals.order_type,
    actuals.order_type_live,
    actuals.order_type_grouped,
    actuals.stage_name,
    actuals.deal_path_name,
    actuals.sales_type,

    -- snapshot info
    actuals.snapshot_date,

    -- flags
    actuals.is_closed,
    actuals.is_won,
    actuals.is_refund,
    actuals.is_downgrade,
    actuals.is_swing_deal,
    actuals.is_edu_oss,
    actuals.is_web_portal_purchase,
    actuals.fpa_master_bookings_flag,
    actuals.is_sao,
    actuals.is_sdr_sao,
    actuals.is_net_arr_closed_deal,
    actuals.is_new_logo_first_order,
    actuals.is_net_arr_pipeline_created_combined,
    actuals.is_win_rate_calc,
    actuals.is_closed_won,
    actuals.is_stage_1_plus,
    actuals.is_stage_3_plus,
    actuals.is_stage_4_plus,
    actuals.is_lost,
    actuals.is_open,
    actuals.is_active,
    actuals.is_credit,
    actuals.is_renewal,
    actuals.is_deleted,
    actuals.is_excluded_from_pipeline_created_combined,
    actuals.created_in_snapshot_quarter_deal_count,
    actuals.is_duplicate,
    actuals.is_contract_reset,
    actuals.is_comp_new_logo_override,
    actuals.is_eligible_open_pipeline_combined,
    actuals.is_eligible_age_analysis_combined,
    actuals.is_eligible_churn_contraction,
    actuals.is_booked_net_arr,
    actuals.is_abm_tier_sao,
    actuals.is_abm_tier_closed_won,
    actuals.primary_solution_architect,
    actuals.product_details,
    actuals.product_category,
    actuals.intended_product_tier,
    actuals.products_purchased,
    actuals.growth_type,
    actuals.opportunity_deal_size,
    actuals.closed_buckets,
    actuals.calculated_deal_size,
    actuals.deal_size,

    -- fields
    actuals.opportunity_name,
    actuals.lead_source,
    actuals.dr_partner_deal_type,
    actuals.dr_partner_engagement,
    actuals.partner_account,
    actuals.dr_status,
    actuals.dr_deal_id,
    actuals.dr_primary_registration,
    actuals.distributor,
    actuals.influence_partner,
    actuals.fulfillment_partner,
    actuals.platform_partner,
    actuals.partner_track,
    actuals.resale_partner_track,
    actuals.is_public_sector_opp,
    actuals.is_registration_from_portal,
    actuals.calculated_discount,
    actuals.partner_discount,
    actuals.partner_discount_calc,
    actuals.comp_channel_neutral,

    --additive fields
    actuals.segment_order_type_iacv_to_net_arr_ratio,
    actuals.calculated_from_ratio_net_arr,
    actuals.net_arr,
    actuals.raw_net_arr,
    actuals.created_and_won_same_quarter_net_arr_combined,
    actuals.new_logo_count,
    actuals.amount,
    actuals.recurring_amount,
    actuals.true_up_amount,
    actuals.proserv_amount,
    actuals.other_non_recurring_amount,
    actuals.arr_basis,
    actuals.arr,
    actuals.count_crm_attribution_touchpoints,
    actuals.weighted_linear_iacv,
    actuals.count_campaigns,
    actuals.probability,
    actuals.days_in_sao,
    actuals.open_1plus_deal_count,
    actuals.open_3plus_deal_count,
    actuals.open_4plus_deal_count,
    actuals.booked_deal_count,
    actuals.churned_contraction_deal_count,
    actuals.open_1plus_net_arr,
    actuals.open_3plus_net_arr,
    actuals.open_4plus_net_arr,
    actuals.booked_net_arr,
    actuals.churned_contraction_net_arr,
    actuals.calculated_deal_count,
    actuals.booked_churned_contraction_deal_count,
    actuals.booked_churned_contraction_net_arr,
    actuals.renewal_amount,
    actuals.total_contract_value,
    actuals.days_in_stage,
    actuals.calculated_age_in_days,
    actuals.days_since_last_activity,
    actuals.pre_military_invasion_arr,
    actuals.won_arr_basis_for_clari,
    actuals.arr_basis_for_clari,
    actuals.forecasted_churn_for_clari,
    actuals.override_arr_basis_clari,
    actuals.vsa_start_date_net_arr,
    actuals.cycle_time_in_days_combined,

    --dates
    dim_date.date_day                                               AS snapshot_day,
    dim_date.day_name                                               AS snapshot_day_name, 
    actuals.snapshot_fiscal_year                                    AS snapshot_fiscal_year,
    actuals.snapshot_fiscal_quarter_name                            AS snapshot_fiscal_quarter_name,
    actuals.snapshot_fiscal_quarter_date                            AS snapshot_fiscal_quarter_date,
    actuals.snapshot_day_of_fiscal_quarter_normalised               AS snapshot_day_of_fiscal_quarter_normalised,
    actuals.snapshot_day_of_fiscal_year_normalised                  AS snapshot_day_of_fiscal_year_normalised,
    dim_date.day_of_week                                            AS snapshot_day_of_week,
    dim_date.first_day_of_week                                      AS snapshot_first_day_of_week,
    dim_date.week_of_year                                           AS snapshot_week_of_year,
    dim_date.day_of_month                                           AS snapshot_day_of_month,
    dim_date.day_of_quarter                                         AS snapshot_day_of_quarter,
    dim_date.day_of_year                                            AS snapshot_day_of_year,
    dim_date.fiscal_quarter                                         AS snapshot_fiscal_quarter,
    dim_date.day_of_fiscal_quarter                                  AS snapshot_day_of_fiscal_quarter,
    dim_date.day_of_fiscal_year                                     AS snapshot_day_of_fiscal_year,
    dim_date.month_name                                             AS snapshot_month_name,
    dim_date.first_day_of_month                                     AS snapshot_first_day_of_month,
    dim_date.last_day_of_month                                      AS snapshot_last_day_of_month,
    dim_date.first_day_of_year                                      AS snapshot_first_day_of_year,
    dim_date.last_day_of_year                                       AS snapshot_last_day_of_year,
    dim_date.first_day_of_quarter                                   AS snapshot_first_day_of_quarter,
    dim_date.last_day_of_quarter                                    AS snapshot_last_day_of_quarter,
    dim_date.first_day_of_fiscal_quarter                            AS snapshot_first_day_of_fiscal_quarter,
    dim_date.last_day_of_fiscal_quarter                             AS snapshot_last_day_of_fiscal_quarter,
    dim_date.first_day_of_fiscal_year                               AS snapshot_first_day_of_fiscal_year,
    dim_date.last_day_of_fiscal_year                                AS snapshot_last_day_of_fiscal_year,
    dim_date.week_of_fiscal_year                                    AS snapshot_week_of_fiscal_year,
    dim_date.month_of_fiscal_year                                   AS snapshot_month_of_fiscal_year,
    dim_date.last_day_of_week                                       AS snapshot_last_day_of_week,
    dim_date.quarter_name                                           AS snapshot_quarter_name,
    dim_date.fiscal_quarter_name_fy                                 AS snapshot_fiscal_quarter_name_fy,
    dim_date.fiscal_quarter_number_absolute                         AS snapshot_fiscal_quarter_number_absolute,
    dim_date.fiscal_month_name                                      AS snapshot_fiscal_month_name,
    dim_date.fiscal_month_name_fy                                   AS snapshot_fiscal_month_name_fy,
    dim_date.holiday_desc                                           AS snapshot_holiday_desc,
    dim_date.is_holiday                                             AS snapshot_is_holiday,
    dim_date.last_month_of_fiscal_quarter                           AS snapshot_last_month_of_fiscal_quarter,
    dim_date.is_first_day_of_last_month_of_fiscal_quarter           AS snapshot_is_first_day_of_last_month_of_fiscal_quarter,
    dim_date.last_month_of_fiscal_year                              AS snapshot_last_month_of_fiscal_year,
    dim_date.is_first_day_of_last_month_of_fiscal_year              AS snapshot_is_first_day_of_last_month_of_fiscal_year,
    dim_date.days_in_month_count                                    AS snapshot_days_in_month_count,
    dim_date.week_of_month_normalised                               AS snapshot_week_of_month_normalised,
    dim_date.week_of_fiscal_quarter_normalised                      AS snapshot_week_of_fiscal_quarter_normalised,
    dim_date.is_first_day_of_fiscal_quarter_week                    AS snapshot_is_first_day_of_fiscal_quarter_week,
    dim_date.days_until_last_day_of_month                           AS snapshot_days_until_last_day_of_month,
    dim_date.current_date_actual                                    AS current_date_actual,
    dim_date.current_fiscal_year                                    AS current_fiscal_year,
    dim_date.current_first_day_of_fiscal_year                       AS current_first_day_of_fiscal_year,
    dim_date.current_fiscal_quarter_name_fy                         AS current_fiscal_quarter_name_fy,
    dim_date.current_first_day_of_month                             AS current_first_day_of_month,
    dim_date.current_first_day_of_fiscal_quarter                    AS current_first_day_of_fiscal_quarter,
    dim_date.current_day_of_month                                   AS current_day_of_month,
    dim_date.current_day_of_fiscal_quarter                          AS current_day_of_fiscal_quarter,
    dim_date.current_day_of_fiscal_year                             AS current_day_of_fiscal_year,
    FLOOR((DATEDIFF(day, dim_date.current_first_day_of_fiscal_quarter, dim_date.current_date_actual) / 7))                   
                                                                    AS current_week_of_fiscal_quarter_normalised,
    created_date.date_actual                                        AS created_date,
    created_date.first_day_of_month                                 AS created_month,
    created_date.first_day_of_fiscal_quarter                        AS created_fiscal_quarter_date,
    created_date.fiscal_quarter_name_fy                             AS created_fiscal_quarter_name,
    created_date.fiscal_year                                        AS created_fiscal_year,
    sales_accepted_date.date_actual                                 AS sales_accepted_date,
    sales_accepted_date.first_day_of_month                          AS sales_accepted_month,
    sales_accepted_date.first_day_of_fiscal_quarter                 AS sales_accepted_fiscal_quarter_date,
    sales_accepted_date.fiscal_quarter_name_fy                      AS sales_accepted_fiscal_quarter_name,
    sales_accepted_date.fiscal_year                                 AS sales_accepted_fiscal_year,
    close_date.date_actual                                          AS close_date,
    close_date.first_day_of_month                                   AS close_month,
    close_date.first_day_of_fiscal_quarter                          AS close_fiscal_quarter_date,
    close_date.fiscal_quarter_name_fy                               AS close_fiscal_quarter_name,
    close_date.fiscal_year                                          AS close_fiscal_year,
    stage_0_pending_acceptance_date.date_actual                     AS stage_0_pending_acceptance_date,
    stage_0_pending_acceptance_date.first_day_of_month              AS stage_0_pending_acceptance_month,
    stage_0_pending_acceptance_date.first_day_of_fiscal_quarter     AS stage_0_pending_acceptance_fiscal_quarter_date,
    stage_0_pending_acceptance_date.fiscal_quarter_name_fy          AS stage_0_pending_acceptance_fiscal_quarter_name,
    stage_0_pending_acceptance_date.fiscal_year                     AS stage_0_pending_acceptance_fiscal_year,
    stage_1_discovery_date.date_actual                              AS stage_1_discovery_date,
    stage_1_discovery_date.first_day_of_month                       AS stage_1_discovery_month,
    stage_1_discovery_date.first_day_of_fiscal_quarter              AS stage_1_discovery_fiscal_quarter_date,
    stage_1_discovery_date.fiscal_quarter_name_fy                   AS stage_1_discovery_fiscal_quarter_name,
    stage_1_discovery_date.fiscal_year                              AS stage_1_discovery_fiscal_year,
    stage_2_scoping_date.date_actual                                AS stage_2_scoping_date,
    stage_2_scoping_date.first_day_of_month                         AS stage_2_scoping_month,
    stage_2_scoping_date.first_day_of_fiscal_quarter                AS stage_2_scoping_fiscal_quarter_date,
    stage_2_scoping_date.fiscal_quarter_name_fy                     AS stage_2_scoping_fiscal_quarter_name,
    stage_2_scoping_date.fiscal_year                                AS stage_2_scoping_fiscal_year,
    stage_3_technical_evaluation_date.date_actual                   AS stage_3_technical_evaluation_date,
    stage_3_technical_evaluation_date.first_day_of_month            AS stage_3_technical_evaluation_month,
    stage_3_technical_evaluation_date.first_day_of_fiscal_quarter   AS stage_3_technical_evaluation_fiscal_quarter_date,
    stage_3_technical_evaluation_date.fiscal_quarter_name_fy        AS stage_3_technical_evaluation_fiscal_quarter_name,
    stage_3_technical_evaluation_date.fiscal_year                   AS stage_3_technical_evaluation_fiscal_year,
    stage_4_proposal_date.date_actual                               AS stage_4_proposal_date,
    stage_4_proposal_date.first_day_of_month                        AS stage_4_proposal_month,
    stage_4_proposal_date.first_day_of_fiscal_quarter               AS stage_4_proposal_fiscal_quarter_date,
    stage_4_proposal_date.fiscal_quarter_name_fy                    AS stage_4_proposal_fiscal_quarter_name,
    stage_4_proposal_date.fiscal_year                               AS stage_4_proposal_fiscal_year,
    stage_5_negotiating_date.date_actual                            AS stage_5_negotiating_date,
    stage_5_negotiating_date.first_day_of_month                     AS stage_5_negotiating_month,
    stage_5_negotiating_date.first_day_of_fiscal_quarter            AS stage_5_negotiating_fiscal_quarter_date,
    stage_5_negotiating_date.fiscal_quarter_name_fy                 AS stage_5_negotiating_fiscal_quarter_name,
    stage_5_negotiating_date.fiscal_year                            AS stage_5_negotiating_fiscal_year,
    stage_6_awaiting_signature_date.date_actual                     AS stage_6_awaiting_signature_date_date,
    stage_6_awaiting_signature_date.first_day_of_month              AS stage_6_awaiting_signature_date_month,
    stage_6_awaiting_signature_date.first_day_of_fiscal_quarter     AS stage_6_awaiting_signature_date_fiscal_quarter_date,
    stage_6_awaiting_signature_date.fiscal_quarter_name_fy          AS stage_6_awaiting_signature_date_fiscal_quarter_name,
    stage_6_awaiting_signature_date.fiscal_year                     AS stage_6_awaiting_signature_date_fiscal_year,
    stage_6_closed_won_date.date_actual                             AS stage_6_closed_won_date,
    stage_6_closed_won_date.first_day_of_month                      AS stage_6_closed_won_month,
    stage_6_closed_won_date.first_day_of_fiscal_quarter             AS stage_6_closed_won_fiscal_quarter_date,
    stage_6_closed_won_date.fiscal_quarter_name_fy                  AS stage_6_closed_won_fiscal_quarter_name,
    stage_6_closed_won_date.fiscal_year                             AS stage_6_closed_won_fiscal_year,
    stage_6_closed_lost_date.date_actual                            AS stage_6_closed_lost_date,
    stage_6_closed_lost_date.first_day_of_month                     AS stage_6_closed_lost_month,
    stage_6_closed_lost_date.first_day_of_fiscal_quarter            AS stage_6_closed_lost_fiscal_quarter_date,
    stage_6_closed_lost_date.fiscal_quarter_name_fy                 AS stage_6_closed_lost_fiscal_quarter_name,
    stage_6_closed_lost_date.fiscal_year                            AS stage_6_closed_lost_fiscal_year,
    subscription_start_date.date_actual                             AS subscription_start_date,
    subscription_start_date.first_day_of_month                      AS subscription_start_month,
    subscription_start_date.first_day_of_fiscal_quarter             AS subscription_start_fiscal_quarter_date,
    subscription_start_date.fiscal_quarter_name_fy                  AS subscription_start_fiscal_quarter_name,
    subscription_start_date.fiscal_year                             AS subscription_start_fiscal_year,
    subscription_end_date.date_actual                               AS subscription_end_date,
    subscription_end_date.first_day_of_month                        AS subscription_end_month,
    subscription_end_date.first_day_of_fiscal_quarter               AS subscription_end_fiscal_quarter_date,
    subscription_end_date.fiscal_quarter_name_fy                    AS subscription_end_fiscal_quarter_name,
    subscription_end_date.fiscal_year                               AS subscription_end_fiscal_year,
    sales_qualified_date.date_actual                                AS sales_qualified_date,
    sales_qualified_date.first_day_of_month                         AS sales_qualified_month,
    sales_qualified_date.first_day_of_fiscal_quarter                AS sales_qualified_fiscal_quarter_date,
    sales_qualified_date.fiscal_quarter_name_fy                     AS sales_qualified_fiscal_quarter_name,
    sales_qualified_date.fiscal_year                                AS sales_qualified_fiscal_year,
    last_activity_date.date_actual                                  AS last_activity_date,
    last_activity_date.first_day_of_month                           AS last_activity_month,
    last_activity_date.first_day_of_fiscal_quarter                  AS last_activity_fiscal_quarter_date,
    last_activity_date.fiscal_quarter_name_fy                       AS last_activity_fiscal_quarter_name,
    last_activity_date.fiscal_year                                  AS last_activity_fiscal_year,
    sales_last_activity_date.date_actual                            AS sales_last_activity_date,
    sales_last_activity_date.first_day_of_month                     AS sales_last_activity_month,
    sales_last_activity_date.first_day_of_fiscal_quarter            AS sales_last_activity_fiscal_quarter_date,
    sales_last_activity_date.fiscal_quarter_name_fy                 AS sales_last_activity_fiscal_quarter_name,
    sales_last_activity_date.fiscal_year                            AS sales_last_activity_fiscal_year,
    technical_evaluation_date.date_actual                           AS technical_evaluation_date,
    technical_evaluation_date.first_day_of_month                    AS technical_evaluation_month,
    technical_evaluation_date.first_day_of_fiscal_quarter           AS technical_evaluation_fiscal_quarter_date,
    technical_evaluation_date.fiscal_quarter_name_fy                AS technical_evaluation_fiscal_quarter_name,
    technical_evaluation_date.fiscal_year                           AS technical_evaluation_fiscal_year,
    arr_created_date.date_actual                                    AS arr_created_date,
    arr_created_date.first_day_of_month                             AS arr_created_month,
    arr_created_date.first_day_of_fiscal_quarter                    AS arr_created_fiscal_quarter_date,
    arr_created_date.fiscal_quarter_name_fy                         AS arr_created_fiscal_quarter_name,
    arr_created_date.fiscal_year                                    AS arr_created_fiscal_year,
    arr_created_date.date_actual                                    AS pipeline_created_date,
    arr_created_date.first_day_of_month                             AS pipeline_created_month,
    arr_created_date.first_day_of_fiscal_quarter                    AS pipeline_created_fiscal_quarter_date,
    arr_created_date.fiscal_quarter_name_fy                         AS pipeline_created_fiscal_quarter_name,
    arr_created_date.fiscal_year                                    AS pipeline_created_fiscal_year,
    arr_created_date.date_actual                                    AS net_arr_created_date,
    arr_created_date.first_day_of_month                             AS net_arr_created_month,
    arr_created_date.first_day_of_fiscal_quarter                    AS net_arr_created_fiscal_quarter_date,
    arr_created_date.fiscal_quarter_name_fy                         AS net_arr_created_fiscal_quarter_name,
    arr_created_date.fiscal_year                                    AS net_arr_created_fiscal_year
  FROM actuals
  INNER JOIN dim_date 
    ON dim_date.date_actual = actuals.snapshot_date
  LEFT JOIN targets 
    ON actuals.actuals_targets_pk = targets.actuals_targets_pk 
  LEFT JOIN dim_date created_date
    ON actuals.created_date_id = created_date.date_id
  LEFT JOIN dim_date sales_accepted_date
    ON actuals.sales_accepted_date_id = sales_accepted_date.date_id
  LEFT JOIN dim_date close_date
    ON actuals.close_date_id = close_date.date_id
  LEFT JOIN dim_date stage_0_pending_acceptance_date
    ON actuals.stage_0_pending_acceptance_date_id = stage_0_pending_acceptance_date.date_id
  LEFT JOIN dim_date stage_1_discovery_date
    ON actuals.stage_1_discovery_date_id = stage_1_discovery_date.date_id
  LEFT JOIN dim_date stage_2_scoping_date
    ON actuals.stage_2_scoping_date_id = stage_2_scoping_date.date_id
  LEFT JOIN dim_date stage_3_technical_evaluation_date
    ON actuals.stage_3_technical_evaluation_date_id = stage_3_technical_evaluation_date.date_id
  LEFT JOIN dim_date stage_4_proposal_date
    ON actuals.stage_4_proposal_date_id = stage_4_proposal_date.date_id
  LEFT JOIN dim_date stage_5_negotiating_date
    ON actuals.stage_5_negotiating_date_id = stage_5_negotiating_date.date_id
  LEFT JOIN dim_date stage_6_awaiting_signature_date
    ON actuals.stage_6_awaiting_signature_date_id = stage_6_awaiting_signature_date.date_id
  LEFT JOIN dim_date stage_6_closed_won_date
    ON actuals.stage_6_closed_won_date_id = stage_6_closed_won_date.date_id
  LEFT JOIN dim_date stage_6_closed_lost_date
    ON actuals.stage_6_closed_lost_date_id = stage_6_closed_lost_date.date_id
  LEFT JOIN dim_date subscription_start_date
    ON actuals.subscription_start_date_id = subscription_start_date.date_id
  LEFT JOIN dim_date subscription_end_date
    ON actuals.subscription_end_date_id = subscription_end_date.date_id
  LEFT JOIN dim_date sales_qualified_date
    ON actuals.sales_qualified_date_id = sales_qualified_date.date_id
  LEFT JOIN dim_date last_activity_date
    ON actuals.last_activity_date_id = last_activity_date.date_id
  LEFT JOIN dim_date sales_last_activity_date
    ON actuals.sales_last_activity_date_id = sales_last_activity_date.date_id
  LEFT JOIN dim_date technical_evaluation_date
    ON actuals.technical_evaluation_date_id = technical_evaluation_date.date_id
  LEFT JOIN dim_date arr_created_date
    ON actuals.arr_created_date_id = arr_created_date.date_id
  
)

SELECT * 
FROM combined
